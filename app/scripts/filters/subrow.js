'use strict';
/**
 * Truncate Filter
 * @Param string
 * @Param int, default = 10
 * @Param string, default = "..."
 * @return string
 */
angular.module('clientSideApp').
    filter('Subrow', function () {
      return function(rows) {
            var flatten = [];
            angular.forEach(rows, function(row){
              subrows = row.subrows;
              flatten.push(row);
              if(subrows){
                angular.forEach(subrows, function(subrow){
                  flatten.push( angular.extend(subrow, {subrow: true}) );
                });
              }
            });
            return flatten;
        }
    });