'use strict';

angular.module('clientSideApp')
  .controller('ProductCtrl', function ($scope, ProductService) {

    $scope.products = ProductService.query();
    $scope.OptCategory = ProductService.getCategory();

    $scope.edit = function(x){
    	$scope.selectedProduct = ProductService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	ProductService.save($scope.selectedProduct).success(function(){
    		$scope.products = ProductService.query();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedProduct = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	ProductService.remove(x).success(function(){
    		$scope.products = ProductService.query();
    		$scope.create();
    	});
    };

    $scope.changedCategory = function() {
        $scope.OptCategoryNew = $scope.OptCategory;
    };
});