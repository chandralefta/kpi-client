'use strict';

angular.module('clientSideApp')
  .controller('VariableCtrl', function ($scope, VariableService) {
    
    $scope.variable = VariableService.query();
    $scope.OptProduct = VariableService.getProduct();
    $scope.OptVarType = VariableService.getVarType();

    $scope.edit = function(x){
    	$scope.selectedVariable = VariableService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	VariableService.save($scope.selectedVariable).success(function(){
    		$scope.variable = VariableService.query();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedVariable = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	VariableService.remove(x).success(function(){
    		$scope.variable = VariableService.query();
    		$scope.create();
    	});
    };

    $scope.changedProduct = function() {
        $scope.OptProductNew = $scope.OptProduct;
    };

    $scope.changedVarType = function() {
        $scope.OptVarTypeNew = $scope.OptVarType;
    };
});