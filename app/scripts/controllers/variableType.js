'use strict';

angular.module('clientSideApp')
  .controller('VariableTypeCtrl', function ($scope, VariableTypeService) {
    
    $scope.variableType = VariableTypeService.query();
    $scope.OptKpiVar = VariableTypeService.getKpiVar();

    $scope.edit = function(x){
    	$scope.selectedVariableType = VariableTypeService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	VariableTypeService.save($scope.selectedVariableType).success(function(){
    		$scope.variableType = VariableTypeService.query();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedVariableType = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	VariableTypeService.remove(x).success(function(){
    		$scope.variableType = VariableTypeService.query();
    		$scope.create();
    	});
    };

    $scope.changedKpiVar = function() {
        $scope.OptKpiVarNew = $scope.OptKpiVar;
    };

});