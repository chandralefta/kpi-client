'use strict';

angular.module('clientSideApp')
  .controller('KpiVariableCtrl', function ($scope, KpiVariableService) {
    
    $scope.kpiVariable = KpiVariableService.query();

    $scope.selectedKpiVariable = {"name": null};


    $scope.edit = function(x){
    	$scope.selectedKpiVariable = KpiVariableService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	KpiVariableService.save($scope.selectedKpiVariable).success(function(){
    		$scope.kpiVariable = KpiVariableService.query();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedKpiVariable = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	KpiVariableService.remove(x).success(function(){
    		$scope.kpiVariable = KpiVariableService.query();
    		$scope.create();
    	});
    };

    $scope.changedProduct = function() {
        $scope.OptProductNew = $scope.OptProduct;
    };

    $scope.changedVarType = function() {
        $scope.OptVarTypeNew = $scope.OptVarType;
    };

});