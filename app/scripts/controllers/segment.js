'use strict';

angular.module('clientSideApp')
  .controller('SegmentCtrl', function ($scope, SegmentService) {
    $scope.segments = SegmentService.query();
    $scope.editSegment = function(x){
    	$scope.selectedSegment = SegmentService.get({id: x.id}, function(result){
            console.log(result);
    		$scope.original = angular.copy(result);
    	});
    };

    $scope.saveSegment = function(){
    	SegmentService.save($scope.selectedSegment).success(function(){
    		$scope.segments = SegmentService.query();
    		$scope.createSegment();
    	});
    };

    $scope.createSegment = function(){
    	$scope.selectedSegment = null;
    	$scope.original = null;
    };

    $scope.deleteSegment = function(x){
    	SegmentService.remove(x).success(function(){
    		$scope.segments = SegmentService.query();
    		$scope.createSegment();
    	});
    };
  });