'use strict';

angular.module('clientSideApp')
  .controller('BranchCtrl', function ($scope, BranchService) {
    $scope.branch = BranchService.query();

    $scope.edit = function(x){
    	$scope.selectedBranch = BranchService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	BranchService.save($scope.selectedBranch).success(function(){
    		$scope.branch = BranchService.query();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedBranch = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	BranchService.remove(x).success(function(){
    		$scope.branch = BranchService.query();
    		$scope.create();
    	});
    };
});