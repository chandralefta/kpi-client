'use strict';

angular.module('clientSideApp')
  .controller('TargetCtrl', ['$scope', '$rootScope', 'TargetAdjService', 'TargetService', function ($scope, $rootScope, TargetAdjService, TargetService) {
    
    $scope.varTarget = TargetService.query();
    $scope.OptVar = TargetService.getVariable();
    $scope.OptBranch = TargetService.getBranch();
    $scope.OptPeriod = TargetService.getPeriod();
    $scope.OptNowPeriod = TargetService.getNowPeriod();
    $scope.targetId = null;

    $scope.Math = window.Math;

    $scope.minAdj = parseFloat(0);
    $scope.maxAdj = parseFloat(1.0);

    $scope.userMinAdj = $scope.minAdj;
    $scope.userMaxAdj = $scope.maxAdj;
   
    $rootScope.selectedTargetAdj = {"period": null, "adjustment": null};

    $scope.$watch('userMaxAdj', function() {
       //$scope.adjustment = {"adjustment": $scope.userMaxAdj}
       //$rootScope.selectedTargetAdj = angular.extend($scope.adjustment, $scope.selectedTargetAdj);
       $rootScope.selectedTargetAdj.adjustment =  $scope.userMaxAdj;
    });

    $scope.action = {"action": "edit"}
    $rootScope.selectedTargetAdj = angular.extend($scope.action, $scope.selectedTargetAdj);

    $scope.edit = function(x){
    	$scope.selectedTarget = TargetService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
              $scope.varTarget = {"varTarget": {"id": $scope.original.id}}
              $rootScope.selectedTargetAdj = angular.extend($scope.varTarget, $scope.selectedTargetAdj);  
              $scope.varTarget = TargetService.query();
    	});
    };

    $scope.save = function(data){
        
        if(data.action == "edit"){
         console.log(data)
          $scope.targetAdjFinal = {"varTarget": data.varTarget, "adjustment": data.adjustment}  
          TargetAdjService.save($scope.targetAdjFinal).success(function(){
            $scope.varTarget = TargetService.query();
            $scope.create();
          });
        }

        if(data.action != "edit"){
            $scope.dataVar = data.var;
            $scope.databranch = data.branch;
            $scope.dataPeriod = {"month": $scope.mySplit(data.period, 1), "year": $scope.mySplit(data.period, 0)};

            $scope.newTarget = {"var": $scope.dataVar, "branch": $scope.databranch, "period": $scope.dataPeriod, "target": data.target}
            console.log($scope.newTarget)
            TargetService.save($scope.newTarget).success(function(){
                $scope.varTarget = TargetService.query();
                $scope.create();
            });
        }

       
    };

    $scope.create = function(){
    	$scope.selectedTarget = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	TargetService.remove(x).success(function(){
    		$scope.varTarget = TargetService.query();
    		$scope.create();
    	});
    };

    $scope.changedVar = function() {
        $scope.OptVarNew = $scope.OptVar;
    };

    $scope.changedBranch = function() {
        $scope.OptBranchNew = $scope.OptBranch;
    };

    $scope.changedPeriod = function() {
        $scope.OptPeriodNew = $scope.OptPeriod;
    };

    $scope.mySplit = function(string, nb) {
        $scope.array = string.split('-');
        return $scope.result = $scope.array[nb];
    };

    $scope.modalShown = false;
    $scope.toggleModal = function() {
      $scope.modalShown = !$scope.modalShown;
    };

}]);