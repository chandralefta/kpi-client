'use strict';

/**
 * @ngdoc function
 * @name clientSideApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the clientSideApp
 */
angular.module('clientSideApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
