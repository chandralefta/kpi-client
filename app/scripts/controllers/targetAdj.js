'use strict';

angular.module('clientSideApp')
  .controller('TargetAdjCtrl', function ($scope, TargetAdjService) {
    
    $scope.varTargetAdj = TargetAdjService.query();
    $scope.OptVar = TargetAdjService.getVariable();
    $scope.OptBranch = TargetAdjService.getBranch();
    $scope.OptPeriod = TargetAdjService.getPeriod();
    $scope.OptNowPeriod = TargetAdjService.getNowPeriod();

    $scope.Math = window.Math;

    $scope.minAdj = parseFloat(0);
    $scope.maxAdj = parseFloat(1.0);

    $scope.userMinAdj = $scope.minAdj;
    $scope.userMaxAdj = $scope.maxAdj;

    $scope.selectedTargetAdj = {"period": null, 
                                "varTarget": null, 
                                "adjustment": null}

    $scope.$watch('userMaxAdj', function() {
       $scope.selectedTargetAdj.adjustment = $scope.userMaxAdj;
    });

    $scope.edit = function(x){
    	$scope.selectedTargetAdj = TargetAdjService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(data){
        console.log(data);
        $scope.dataVar = data.var;
        $scope.databranch = data.branch;
        $scope.dataPeriod = {"month": $scope.mySplit(data.period, 1), "year": $scope.mySplit(data.period, 0)};

        $scope.newTarget = {"var": $scope.dataVar, "branch": $scope.databranch, "period": $scope.dataPeriod, "target": data.target}
        console.log($scope.newTarget)
        TargetAdjService.save($scope.newTarget).success(function(){
            $scope.varTargetAdj = TargetAdjService.query();
            $scope.create();
        });
    };

    $scope.create = function(){
    	$scope.selectedTargetAdj = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	TargetAdjService.remove(x).success(function(){
    		$scope.varTargetAdj = TargetAdjService.query();
    		$scope.create();
    	});
    };

    $scope.changedVar = function() {
        $scope.OptVarNew = $scope.OptVar;
    };

    $scope.changedBranch = function() {
        $scope.OptBranchNew = $scope.OptBranch;
    };

    $scope.changedPeriod = function() {
        $scope.OptPeriodNew = $scope.OptPeriod;
    };

    $scope.mySplit = function(string, nb) {
        $scope.array = string.split('-');
        return $scope.result = $scope.array[nb];
    };

    $scope.modalShown = false;
    $scope.toggleModal = function() {
      $scope.modalShown = !$scope.modalShown;
    };

});