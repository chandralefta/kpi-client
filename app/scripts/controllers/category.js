'use strict';

angular.module('clientSideApp')
  .controller('CategoryCtrl', function ($scope, CategoryService) {
    
    $scope.category = CategoryService.query();

    $scope.edit = function(x){
    	$scope.selectedCategory = CategoryService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	CategoryService.save($scope.selectedCategory).success(function(){
    		$scope.category = CategoryService.query();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedCategory = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	CategoryService.remove(x).success(function(){
    		$scope.category = CategoryService.query();
    		$scope.create();
    	});
    };
});