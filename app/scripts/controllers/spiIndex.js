'use strict';

angular.module('clientSideApp')
  .controller('SpiIndexCtrl', function ($scope, SpiIndexService) {
    $scope.spii = SpiIndexService.query();
    $scope.editSpiIndex = function(x){
    	$scope.selectedSpiIndex = SpiIndexService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.saveSpiIndex = function(){
    	SpiIndexService.save($scope.selectedSpiIndex).success(function(){
    		$scope.spii = SpiIndexService.query();
    		$scope.createSpiIndex();
    	});
    };

    $scope.createSpiIndex = function(){
    	$scope.selectedSpiIndex = null;
    	$scope.original = null;
    };

    $scope.deleteSpiIndex = function(x){
    	SpiIndexService.remove(x).success(function(){
    		$scope.spii = SpiIndexService.query();
    		$scope.createSpiIndex();
    	});
    };
});