'use strict';

angular.module('clientSideApp')
  .controller('AchievementCtrl', function ($scope, AchievementService) {
    
    $scope.varAchievement = AchievementService.query();
    $scope.OptVar = AchievementService.getVariable();
    $scope.OptBranch = AchievementService.getBranch();
    $scope.OptPeriod = AchievementService.getPeriod();
    $scope.OptNowPeriod = AchievementService.getNowPeriod();

    $scope.edit = function(x){
    	$scope.selectedAchievement = AchievementService.get({id: x.id}, function(result){
    		  $scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(data){
        $scope.dataVar = data.var;
        $scope.databranch = data.branch;
        // $scope.varTarget = {};

        var currentTime = new Date()

        var month = currentTime.getMonth() + 1

        var year = currentTime.getFullYear()
        
        $scope.selectedSaveAchievement = AchievementService.getVar({id: $scope.dataVar.id}, function(result){
              $scope.originalSave = angular.copy(result);
        });
       
        $scope.varTargetOld = AchievementService.getTarget(function(response) {
            angular.forEach(response, function(item) {
                 
                if ((item.period.month == month && item.period.year == year) && 
                    (item.var.name == $scope.selectedSaveAchievement.name)) {
                    $scope.varTarget = {"id": item.id };
                    $scope.newAchievement = {"var": $scope.dataVar, "branch": $scope.databranch, "varTarget": $scope.varTarget, "achievement": data.achievement}
                    console.log($scope.newAchievement);
                    AchievementService.save($scope.newAchievement).success(function(){
                        $scope.varAchievement = AchievementService.query();
                        $scope.create();
                    });
                }
            });
        });

      
    };

    $scope.create = function(){
    	$scope.selectedAchievement = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	AchievementService.remove(x).success(function(){
    		$scope.varAchievement = AchievementService.query();
    		$scope.create();
    	});
    };

    $scope.changedVar = function() {
        $scope.OptVarNew = $scope.OptVar;
    };

    $scope.changedBranch = function() {
        $scope.OptBranchNew = $scope.OptBranch;
    };

    $scope.changedPeriod = function() {
        $scope.OptPeriodNew = $scope.OptPeriod;
    };

    $scope.mySplit = function(string, nb) {
        $scope.array = string.split('-');
        return $scope.result = $scope.array[nb];
    };

    $scope.modalShown = false;
    $scope.toggleModal = function() {
      $scope.modalShown = !$scope.modalShown;
    };

});