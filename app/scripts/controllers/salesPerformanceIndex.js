'use strict';

angular.module('clientSideApp')
  .controller('SalesPerformanceIndexCtrl', ['$scope', '$location', 'SalesPerformanceIndexService', function ($scope, $location, SalesPerformanceIndexService) {
    $scope.spi = SalesPerformanceIndexService.getSpiNew();
    $scope.OptBranch = SalesPerformanceIndexService.getBranch();
    $scope.Products = SalesPerformanceIndexService.getProduct();
  


    $scope.edit = function(x){
    	$scope.selectedSpi = SalesPerformanceIndexService.get({id: x.id}, function(result){
    		$scope.original = angular.copy(result);
    	});
    };

    $scope.save = function(){
    	SalesPerformanceIndexService.save($scope.selectedSpi).success(function(){
    		$scope.spi = SalesPerformanceIndexService.getSpiNew();
    		$scope.create();
    	});
    };

    $scope.create = function(){
    	$scope.selectedSpi = null;
    	$scope.original = null;
    };

    $scope.delete = function(x){
    	SalesPerformanceIndexService.remove(x).success(function(){
    		$scope.spi = SalesPerformanceIndexService.getSpiNew();
    		$scope.create();
    	});
    };

    $scope.changed = function() {
        $scope.OptBranchNew = $scope.OptBranch;
    }

    $scope.filterSpi = function(){
        SalesPerformanceIndexService.filterByBranchAndMonth({branchId: $scope.OptBranchNew, periodeDate: $scope.periodeDate}, function(result){
            $scope.filterSpiData =  angular.copy(result);
            console.log($scope.filterSpiData);
        });
    };

    $scope.go = function ( path ) {
      $location.path( path );
    };

    $scope.filterDataSpi = function(){
         $scope.spi = SalesPerformanceIndexService.getSpiNew();
         $scope.varTarget = SalesPerformanceIndexService.getTarget();

         $scope.newTarget = {0 : $scope.spi,  1 : $scope.varTarget}

         console.log($scope.newTarget);
    };

    $scope.spis = $scope.filterDataSpi();

  }]);