'use strict';

angular.module('clientSideApp')
	.factory('SpiIndexService', function($resource, $http){
		return {
	      spiIndexResource: $resource('kpi/spi_index/:id'),
	      get: function(param, callback){
	        return this.spiIndexResource.get(param,callback);
	      },
	      query: function(){
	        return this.spiIndexResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/spi_index/'+data.id, data);
	        } else {
	        	return $http.post('kpi/spi_index', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/spi_index/'+data.id);
	        }
	      }
	   };
});