
'use strict';

angular.module('clientSideApp')
	.factory('TargetService', function($resource, $http){
		return {
	      targetResource: $resource('kpi/target/:id'),
	      get: function(param, callback){
	        return this.targetResource.get(param,callback);
	      },
	      query: function(){
	        return this.targetResource.query();
	      },
	      save: function(data){
	      	console.log(data);
	        if(data.id != null){
	        	return $http.put('kpi/target/'+data.id, data);
	        } else {
	        	return $http.post('kpi/target', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/target/'+data.id);
	        }
	      },

	      getVariableResource: $resource('kpi/var/:id'),
	      getVariable: function(){
	      	 return this.getVariableResource.query();
	      },

	      getBranchResource: $resource('kpi/branch/:id'),
	      getBranch: function(){
	      	 return this.getBranchResource.query();
	      },

	      getPeriodResource: $resource('kpi/period/:id'),
	      getPeriod: function(){
	      	 return this.getPeriodResource.query();
	      },

	      getNowPeriodResource: $resource('kpi/spi_period'),
	      getNowPeriod: function(){
	      	 return this.getNowPeriodResource.query();
	      }
	   };
});