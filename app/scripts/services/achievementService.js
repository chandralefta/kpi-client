
'use strict';

angular.module('clientSideApp')
	.factory('AchievementService', function($resource, $http){
		return {
	      achievementResource: $resource('kpi/achievement/:id'),
	      get: function(param, callback){
	        return this.achievementResource.get(param,callback);
	      },
	      query: function(){
	        return this.achievementResource.query();
	      },
	      save: function(data){
	      	// console.log(data);
	        if(data.id != null){
	        	return $http.put('kpi/achievement/'+data.id, data);
	        } else {
	        	return $http.post('kpi/achievement', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/achievement/'+data.id);
	        }
	      },

	      getVariableResource: $resource('kpi/var/:id'),
	      getVariable: function(){
	      	 return this.getVariableResource.query();
	      },

	      getBranchResource: $resource('kpi/branch/:id'),
	      getBranch: function(){
	      	 return this.getBranchResource.query();
	      },

	      getPeriodResource: $resource('kpi/period/:id'),
	      getPeriod: function(){
	      	 return this.getPeriodResource.query();
	      },

	      getNowPeriodResource: $resource('kpi/spi_period'),
	      getNowPeriod: function(){
	      	 return this.getNowPeriodResource.query();
	      },

	      getTargetResource: $resource('kpi/target'),
	      getTarget: function(callback){
	      	return  this.getTargetResource.query(callback);
	      },

	      getVarResource: $resource('kpi/var/:id'),
	      getVar: function(param, callback){
	        return this.getVarResource.get(param,callback);
	      }
	   };
});