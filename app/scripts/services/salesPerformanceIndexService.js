'use strict';

angular.module('clientSideApp')
	.factory('SalesPerformanceIndexService', function($resource, $http){
		return {
	      salesPerformanceIndexResource: $resource('kpi/spi/:id'),
	      get: function(param, callback){
	        return this.salesPerformanceIndexResource.get(param,callback);
	      },
	      query: function(){
	        return this.salesPerformanceIndexResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/spi/'+data.id, data);
	        
	        } else {
	        	return $http.post('kpi/spi', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/spi/'+data.id);
	        }
	      },
	      getBranchResource: $resource('kpi/branch/:id'),
	      getBranch: function(){
	      	 return this.getBranchResource.query();
	      },
	      spiFilterResource: $resource('kpi/spi_filter/:branchId/:periodeDate'),
	      filterByBranchAndMonth: function(param, callback){
	        return this.spiFilterResource.get(param,callback);
	      },
	      getProductResource: $resource('kpi/product/:id'),
	      getProduct: function(){
	      	 return this.getProductResource.query();
	      },
	      getSpiNewResource: $resource('kpi/spi_new/:id'),
	      getSpiNew: function(){
	      	 return this.getSpiNewResource.query();
	      },

	      getTargetResource: $resource('kpi/target/:id'),
	      getTarget: function(){
	          var spiNew = this.getSpiNewResource.query();
	      	  var spiTarget = this.getTargetResource.query();
			  var spiNewObj = {};
			  var spiTargetObj = {};
			  var spiData = [];
				for (var i = 0, l = spiNew.length; i < l; i++) {
				  spi[i.toString()] = spiNew[i];
				}

				for (var i = 0, l = spiTarget.length; i < l; i++) {
				  spiTargetObj[i.toString()] = spiTarget[i];
				}

				
			  spiData[0] = spiNewObj;
			  spiData[1] = spiTargetObj;
				
				console.log(spiData);
	      	  return null;
	      }
	   };
});