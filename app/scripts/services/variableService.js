
'use strict';

angular.module('clientSideApp')
	.factory('VariableService', function($resource, $http){
		return {
	      variableResource: $resource('kpi/var/:id'),
	      get: function(param, callback){
	        return this.variableResource.get(param,callback);
	      },
	      query: function(){
	        return this.variableResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/var/'+data.id, data);
	        } else {
	        	return $http.post('kpi/var', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/var/'+data.id);
	        }
	      },

	      getProductResource: $resource('kpi/product/:id'),
	      getProduct: function(){
	      	 return this.getProductResource.query();
	      },

	      getVarTypeResource: $resource('kpi/var_type/:id'),
	      getVarType: function(){
	      	 return this.getVarTypeResource.query();
	      }
	   };
});