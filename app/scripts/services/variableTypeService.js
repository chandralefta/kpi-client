
'use strict';

angular.module('clientSideApp')
    .factory('VariableTypeService', function($resource, $http){
        return {
          variableTypeResource: $resource('kpi/var_type/:id'),
          get: function(param, callback){
            return this.variableTypeResource.get(param,callback);
          },
          query: function(){
            return this.variableTypeResource.query();
          },
          save: function(data){
            if(data.id != null){
                return $http.put('kpi/var_type/'+data.id, data);
            } else {
                return $http.post('kpi/var_type', data);
            }
          },
          remove: function(data){
            if(data.id != null){
              return $http.delete('kpi/var_type/'+data.id);
            }
          },

          getKpiVarResource: $resource('kpi/kpi_var/:id'),
          getKpiVar: function(){
             return this.getKpiVarResource.query();
          }
       };
});