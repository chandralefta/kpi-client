'use strict';

angular.module('clientSideApp')
	.factory('ProductService', function($resource, $http){
		return {
	      productResource: $resource('kpi/product/:id'),
	      get: function(param, callback){
	        return this.productResource.get(param,callback);
	      },
	      query: function(){
	        return this.productResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/product/'+data.id, data);
	        } else {
	        	return $http.post('kpi/product', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/product/'+data.id);
	        }
	      },

	      getCategoryResource: $resource('kpi/category/:id'),
	      getCategory: function(){
	      	 return this.getCategoryResource.query();
	      }
	   };
});