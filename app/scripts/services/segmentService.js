'use strict';

angular.module('clientSideApp')
	.factory('SegmentService', function($resource, $http){
		return {
	      segmentResource: $resource('kpi/segment/:id'),
	      get: function(param, callback){
	        return this.segmentResource.get(param,callback);
	      },
	      query: function(){
	        return this.segmentResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/segment/'+data.id, data);
	     
	        } else {
	        	console.log(data);
	        	return $http.post('kpi/segment', data);
	        }
	      },
	      remove: function(data){
	        if(data.id !== null){
	          return $http.delete('kpi/segment/'+data.id);
	        }
	      }
	   };
});