
'use strict';

angular.module('clientSideApp')
	.factory('KpiVariableService', function($resource, $http){
		return {
	      kpiVariableResource: $resource('kpi/kpi_var/:id'),
	      get: function(param, callback){
	        return this.kpiVariableResource.get(param,callback);
	      },
	      query: function(){
	        return this.kpiVariableResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/kpi_var/'+data.id, data);
	        } else {
	        	return $http.post('kpi/kpi_var', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/kpi_var/'+data.id);
	        }
	      }
	      
	   };
});