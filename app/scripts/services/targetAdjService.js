
'use strict';

angular.module('clientSideApp')
	.factory('TargetAdjService', function($resource, $http){
		return {
	      targetAdjResource: $resource('kpi/target_adj/:id'),
	      get: function(param, callback){
	        return this.targetAdjResource.get(param,callback);
	      },
	      query: function(){
	        return this.targetAdjResource.query();
	      },
	      save: function(data){
	      	//console.log(data);
	        if(data.id != null){
	        	return $http.put('kpi/target_adj/'+data.id, data);
	        } else {
	        	return $http.post('kpi/target_adj', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/target_adj/'+data.id);
	        }
	      },

	      getVariableResource: $resource('kpi/var/:id'),
	      getVariable: function(){
	      	 return this.getVariableResource.query();
	      },

	      getBranchResource: $resource('kpi/branch/:id'),
	      getBranch: function(){
	      	 return this.getBranchResource.query();
	      },

	      getPeriodResource: $resource('kpi/period/:id'),
	      getPeriod: function(){
	      	 return this.getPeriodResource.query();
	      },

	      getNowPeriodResource: $resource('kpi/spi_period'),
	      getNowPeriod: function(){
	      	 return this.getNowPeriodResource.query();
	      }
	   };
});