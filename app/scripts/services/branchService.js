'use strict';

angular.module('clientSideApp')
	.factory('BranchService', function($resource, $http){
		return {
	      branchResource: $resource('kpi/branch/:id'),
	      get: function(param, callback){
	        return this.branchResource.get(param,callback);
	      },
	      query: function(){
	        return this.branchResource.query();
	      },
	      save: function(data){
	        if(data.id != null){
	        	return $http.put('kpi/branch/'+data.id, data);
	        } else {
	        	return $http.post('kpi/branch', data);
	        }
	      },
	      remove: function(data){
	        if(data.id != null){
	          return $http.delete('kpi/branch/'+data.id);
	        }
	      }
	   };
});