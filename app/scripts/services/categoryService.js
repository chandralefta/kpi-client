
'use strict';

angular.module('clientSideApp')
    .factory('CategoryService', function($resource, $http){
        return {
          categoryResource: $resource('kpi/category/:id'),
          get: function(param, callback){
            return this.categoryResource.get(param,callback);
          },
          query: function(){
            return this.categoryResource.query();
          },
          save: function(data){
            if(data.id != null){
                return $http.put('kpi/category/'+data.id, data);
            } else {
                return $http.post('kpi/category', data);
            }
          },
          remove: function(data){
            if(data.id != null){
              return $http.delete('kpi/category/'+data.id);
            }
          }
       };
});