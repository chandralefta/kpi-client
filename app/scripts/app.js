'use strict';

/**
 * @ngdoc overview
 * @name clientSideApp
 * @description
 * # clientSideApp
 *
 * Main module of the application.
 */
angular
  .module('clientSideApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.date',
    'ui-rangeSlider'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/branch', {
        templateUrl: 'views/branch/index.html',
        controller: 'BranchCtrl'
      })
      .when('/spi', {
        templateUrl: 'views/sales_performance_index/index.html',
        controller: 'SalesPerformanceIndexCtrl'
      })
      .when('/spi_new', {
        templateUrl: 'views/sales_performance_index/new.html',
        controller: 'SalesPerformanceIndexCtrl'
      })
      .when('/segment', {
        templateUrl: 'views/segment/index.html',
        controller: 'SegmentCtrl'
      })
      .when('/spi_index', {
        templateUrl: 'views/spi_index/index.html',
        controller: 'SpiIndexCtrl'
      })
      .when('/var', {
        templateUrl: 'views/variable/index.html',
        controller: 'VariableCtrl'
      })
      .when('/kpi_var', {
        templateUrl: 'views/kpi_variable/index.html',
        controller: 'KpiVariableCtrl'
      })
      .when('/var_type', {
        templateUrl: 'views/variable_type/index.html',
        controller: 'VariableTypeCtrl'
      })
      .when('/category', {
        templateUrl: 'views/category/index.html',
        controller: 'CategoryCtrl'
      })
      .when('/product', {
        templateUrl: 'views/product/index.html',
        controller: 'ProductCtrl'
      })
      .when('/target', {
        templateUrl: 'views/target/index.html',
        controller: 'TargetCtrl'
      })
      .when('/achievement', {
        templateUrl: 'views/achievement/index.html',
        controller: 'AchievementCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
