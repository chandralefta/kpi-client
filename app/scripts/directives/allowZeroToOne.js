app.directive('allowZeroToOne',function(){
  return {
    restrict: 'A',
    require: 'ngModel',
    link: function(scope, el, attrs, ngModelCtrl){
      function fromUser(text) {
        var old = ngModelCtrl.$modelValue;
        var f = parseFloat(text);
        if(RegExp(/^(-?0*\d(\.\d*)?)$/).test(text) && (f===-1 || (f >= 0 && f <= 1)) || text==='' || text==='-') {
          return text;
        }else{
          ngModelCtrl.$setViewValue(old);
          ngModelCtrl.$render();
          return old;
        }
      }
      ngModelCtrl.$parsers.push(fromUser);
    }
  }
});