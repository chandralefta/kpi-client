'use strict';

/**
 * @ngdoc directive
 * @name clientSideApp.directive:target
 * @description
 * # target
 */
angular.module('clientSideApp')
  .directive('TargetSelector', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/sales_performance_index/target.html'
      // link: function postNew(scope, element, attrs) {
      //    element.text('this is the target directive');
      // }
    };
  });
