'use strict';

/**
 * @ngdoc directive
 * @name clientSideApp.directive:achievement
 * @description
 * # achievement
 */
angular.module('clientSideApp')
  .directive('achievement', function () {
    return {
      restrict: 'E',
      templateUrl: 'views/sales_performance_index/achievement.html'
      // link: function postLink(scope, element, attrs) {
      //   element.text('this is the achievement directive');
      // }
    };
  });
